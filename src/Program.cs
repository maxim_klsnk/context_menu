﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using Peter;

namespace context_menu
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                MessageBox.Show("Usage: 'context_menu <path_to_file_or_folder>'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string targetPath = args[0];

            if (!Directory.Exists(targetPath) && !File.Exists(targetPath))
            {
                string errorMessage = String.Format("Path '{0}' doesn't exist", targetPath);
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            ShellContextMenu ctxMnu = new ShellContextMenu();
            FileInfo[] arrFI = new FileInfo[1];
            arrFI[0] = new FileInfo(targetPath);
            ctxMnu.ShowContextMenu(arrFI, new Point(100, 100));
        }
    }
}
